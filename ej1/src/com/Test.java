package com;

import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class Test {

    public static void main(String[] args) throws IOException {
        
        PdfReader obj = new PdfReader("C:\\Users\\Emmet\\Downloads\\DV00106A Mineria de datos data mining modelos tecnicas herramientas 2.pdf");
        
        int ndp = obj.getNumberOfPages();
        System.out.println("Numero de paginas del documento: " + ndp);
        System.out.println("\n-----------------------------------------");
        
        int version = obj.getPdfVersion();
        System.out.println("Version " + version);
        System.out.println("\n-----------------------------------------");
        
        //String pagina = PdfTextExtractor.getTextFromPage(obj, 1);
        String paginas = "";
        for (int i = 1; i < ndp; i++) {
            paginas = paginas + PdfTextExtractor.getTextFromPage(obj, i);
        }
        System.out.println("Contenido: \n" + paginas);
        System.out.println("\n-----------------------------------------");

        Map<String, String> info = obj.getInfo();
        System.out.println("\ninformacion del pdf: " + info);
        System.out.println("\n-----------------------------------------");
        long longitud = obj.getFileLength();
        System.out.println("\n\nlongitud del contenido del pdf: " + longitud);
        System.out.println("\n-----------------------------------------");
        //prueba
        /*String cadena = "Hola que tal, el otro dia iba caminando por la calle, derrepen-"
                + "te se obscurecio, no se porque pero el tiempo volo demaciado rapido.";
        String cadena2 = cadena;*/
        
        String delimitadores = "[ .,;?!��\'\"\\[\\]]+";
        String[] palabrasSeparadas = paginas.toLowerCase().split(delimitadores);
        //quitar el guion a palabras continuadas por guion
        System.out.println("lista de palabras: \n");
        for (int i = 0; i < palabrasSeparadas.length; i++) {
            int b = palabrasSeparadas[i].indexOf('-');
            if (b != -1) {
                palabrasSeparadas[i] = palabrasSeparadas[i].substring(0, b).toLowerCase() + palabrasSeparadas[i].substring(b + 1, palabrasSeparadas[i].length()).toLowerCase();
            }
            System.out.println(palabrasSeparadas[i]);
        }
        
        System.out.println("\n-----------------------------------------");
        
        Map<String, Integer> map = new TreeMap<String, Integer>();
        for (int i = 0; i < palabrasSeparadas.length; i++) {
            if (map.containsKey(palabrasSeparadas[i])) {
                map.put(palabrasSeparadas[i], (map.get(palabrasSeparadas[i])+1));
            }else{
                map.put(palabrasSeparadas[i], 1);
            }
        }
        System.out.println("\n\n\n\n");
        System.out.println("frecuencia de palabras: \n");
        System.out.println(map.entrySet());
        
    }//end main
    //leer el pdf, obtener las palabras y la frecuencia
    //cuantas palabras hay diferentes el documento
    //frecuencia de cada palabra
    //repositorio en bitbucket se debe llamar tarea1
    //utilizar sourcetree para windows, para linux en git
}
